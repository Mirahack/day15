# O

Today we learned about agile development, including the roles of agile development, such as BA, OA, UX, DEV, and TL. We also learned about activities in agile development, such as standup meetings, IPM, Story Kickoff, Desk check, Code Review, and Retrospective. All of this is in preparation for next week's simulation project.

# R

I am in high spirits.

# I

These weeks of learning have laid the foundation for our simulation project next week. It can be said that next week's simulation project is a time to test our learning achievements and also a time to test our team cohesion. After work, our team made some internal planning, and now I can't wait to start practicing.

# D

I hope that in the coming week, we can make good use of the knowledge we have learned, use agile development patterns to develop a project that satisfies everyone, and submit a satisfactory answer sheet in the final presentation.